const index = require('../index'); // Import index to run rl on this file

// Function to calculate Sphere volume
function calculateVolumeSphere(number) {
  return (4*Math.PI*number*number*number)/3;
}

// Function to input the Radius
function input() {
  index.rl.question(`Input Radius: `, (number) => {
    if (!isNaN(number) && !index.isEmptyOrSpaces(number)) {
      console.log(`Sphere's volume is ${calculateVolumeSphere(number)}\n`);
      index.rl.close();
    } else {
      console.log(`Radius must be number`);
      input();
    }
  });
}

module.exports = { input }; // Export the input, so the another file can run this code

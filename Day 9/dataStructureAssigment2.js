let statusPasien = [
    {
        Name: 'Jono',
        Status: 'Positive'
    },
    {
        Name: 'Joni',
        Status: 'Suspect'
    },
    {
        Name: 'Jona',
        Status: 'Negative'
    },
    {
        Name: 'Jone',
        Status: 'Positive'
    },
    {
        Name: 'Jene',
        Status: 'Positive'
    },
    {
        Name: 'Jana',
        Status: 'Suspect'
    },
    {
        Name: 'Jini',
        Status: 'Negative'
    },
    {
        Name: 'Junu',
        Status: 'Positive'
    },
];

let statusPasienPositif = statusPasien.filter((pasien) => 
    pasien.Status.includes('Positive')
)
let statusPasienNegative = statusPasien.filter((pasien) => 
    pasien.Status.includes('Negative')
)
let statusPasienSuspect = statusPasien.filter((pasien) => 
    pasien.Status.includes('Suspect')
)

console.log(statusPasienPositif);
console.log(statusPasienNegative);
console.log(statusPasienSuspect);

//I don't have idea to make the data pasien out with switch case,
// so I make it like this.
// I think I missunderstanding with the instruction?Syorry:>
